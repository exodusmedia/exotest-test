<?php
/**
 * Test the ExoTest Libraries Work
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
include('ExoTest/Suite.php');
use ExoTest\SelfTest\Suite;

// begin the suite, test yourself
$suite = new Suite();
$suite->execute();
print($suite->display_results());
